#pragma once

#ifndef CLIENT_ID
#error "CLIENT_ID must be defined"
#endif  // !CLIENT_ID

#ifndef DETAILS
#define DETAILS NULL
#endif  // !DETAILS

#ifndef STATE
#define STATE NULL
#endif  // !STATE

#ifndef LARGE_IMAGE
#define LARGE_IMAGE NULL
#endif  // !LARGE_IMAGE

#ifndef LARGE_TEXT
#define LARGE_TEXT NULL
#endif  // !LARGE_TEXT

#ifndef SMALL_IMAGE
#define SMALL_IMAGE NULL
#endif  // !SMALL_IMAGE

#ifndef SMALL_TEXT
#define SMALL_TEXT NULL
#endif  // !SMALL_TEXT
